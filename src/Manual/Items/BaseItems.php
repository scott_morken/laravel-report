<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 3/2/15
 * Time: 9:19 AM
 */

namespace Smorken\Report\Manual\Items;

use Smorken\Report\Manual\Contracts\Items;

class BaseItems implements Items
{

    protected $items;

    public function setItems($items)
    {
        $this->items = $items;
    }

    public function getItems()
    {
        return $this->items;
    }
}