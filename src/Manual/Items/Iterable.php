<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 3/2/15
 * Time: 9:21 AM
 */

namespace Smorken\Report\Manual\Items;


class Iterable extends BaseItems
{

    public function __construct($items = array())
    {
        $this->setItems($items);
    }
}