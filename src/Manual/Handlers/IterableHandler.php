<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 3/2/15
 * Time: 8:43 AM
 */

namespace Smorken\Report\Manual\Handlers;

use Smorken\Report\Manual\Contracts\Handler;
use Smorken\Report\Manual\Contracts\Items;
use Smorken\Report\Manual\Contracts\Model;

class IterableHandler implements Handler
{

    /**
     * @var Items
     */
    protected $items;

    /**
     * @param Items $items
     */
    public function __construct(Items $items)
    {
        $this->linkItems($items);
    }

    public function run($voclass, $value_callback, $key_callback = null, $vo_callback = null)
    {
        $results = array();
        foreach($this->items->getItems() as $k => $v) {
            $valueresult = $this->handleCallback($value_callback, array($k, $v, &$results));
            if ($valueresult !== false) {
                $keyresult = 0;
                if (!is_null($key_callback)) {
                    $keyresult = $this->handleCallback($key_callback, array($voclass, $k, $v, &$results));
                }
                $vo = $this->getVOFromKeyResult($keyresult, $voclass, $results);
                $voresult = true;
                if (!is_null($vo_callback)) {
                    $voresult = $this->handleCallback($vo_callback, array($k, $v, $vo, &$results));
                }
                if ($voresult) {
                    $vo->add($valueresult);
                }
            }
        }
        return $results;
    }

    protected function getVOFromKeyResult($keyresult, $voclass, &$results)
    {
        if ($keyresult instanceof Model) {
            $vo = $keyresult;
        }
        elseif (!isset($results[$keyresult])) {
            $vo = new $voclass;
            $results[$keyresult] = $vo;
        }
        else {
            $vo = $results[$keyresult];
        }
        return $vo;
    }

    protected function handleCallback($callback, $params = array())
    {
        if (is_string($callback)) {
            $callback = array($this, $callback);
        }
        return call_user_func_array($callback, $params);
    }

    public function linkItems(Items $items)
    {
        $this->items = $items;
    }
}