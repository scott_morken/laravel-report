<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 3/2/15
 * Time: 8:40 AM
 */

namespace Smorken\Report\Manual\Contracts;


interface Result
{
    public function run($type = 'default');

    public function addTypes($types = array());

    public function addType($type, $voclass, $value_cb, $key_cb = null, $vo_cb = null);

    public function getType($type = 'default');

    /**
     * @return array
     */
    public function getTypes();

    public function getDefaultType();

    /**
     * @return Handler
     */
    public function getHandler();

    /**
     * @param Handler $handler
     */
    public function setHandler(Handler $handler);
}