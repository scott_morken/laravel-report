<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 3/2/15
 * Time: 8:38 AM
 */

namespace Smorken\Report\Manual\Contracts;


interface Handler
{
    /**
     * @param Model $voclass class name to create for the VO
     * @param $value_callback callback to run with $key, $value to parse value, return false to skip row
     * @param null $key_callback callback to run with $key, $value to create a key, if null, key is 0
     * @param null $vo_callback callback to run with $vo, $key, $value to add info to $vo
     * @return array
     */
    public function run($voclass, $value_callback, $key_callback = null, $vo_callback = null);

    /**
     * @param Items $items
     */
    public function linkItems(Items $items);
}