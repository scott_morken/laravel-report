<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 3/2/15
 * Time: 8:39 AM
 */

namespace Smorken\Report\Manual\Contracts;


interface Model
{
    public function getLabel();

    public function setLabel($label);

    public function getData();

    public function setData($data);

    public function getValues();

    public function setValues($values);

    public function add($value);

    public function sum();

    public function min();

    public function max();

    public function count();

    public function avg();
}