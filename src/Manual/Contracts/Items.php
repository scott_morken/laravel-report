<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 3/2/15
 * Time: 9:18 AM
 */

namespace Smorken\Report\Manual\Contracts;


interface Items {

    public function setItems($items);

    public function getItems();

}