<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 3/2/15
 * Time: 8:29 AM
 */

namespace Smorken\Report\Manual\Results;


use Smorken\Report\Manual\Contracts\Handler;
use Smorken\Report\Manual\Contracts\Result as ResultContract;

class Result implements ResultContract
{

    /**
     * An array of key => values for the handler to parse
     * [
     *   'sum_by_foo' => [
     *     'voclass' => 'Smorken\Report\Manual\Models\VO',
     *     'value_callback' => 'valueCallback', //methodName on $this or a callable
     *     'key_callback' => 'keyCallback',
     *     'vo_callback' => 'voCallback',
     *   ]
     * ]
     * @var array
     */
    protected $types = array();

    /**
     * @var
     */
    protected $handler;

    public function __construct(Handler $handler, $types = array())
    {
        $this->setHandler($handler);
        if ($types) {
            $this->addTypes($types);
        }
    }

    public function run($type = 'default')
    {
        $params = $this->getType($type);
        if ($this->getHandler()) {
            $handler = $this->getHandler();
            return call_user_func_array(array($handler, 'run'), $params);
        }
    }

    public function addTypes($types = array())
    {
        $defaults = [
            'voclass' => 'Smorken\Report\Manual\Models\VO',
            'value_callback' => function($k, $v) { return floatval($v); },
            'key_callback' => null,
            'vo_callback' => null,
        ];
        foreach($types as $k => $v) {
            $params = [$k];
            foreach($defaults as $dk => $dv) {
                if (isset($v[$dk])) {
                    $params[] = $v[$dk];
                }
                else {
                    $params[] = $dv;
                }
            }
            call_user_func_array(array($this, 'addType'), $params);
        }
    }

    public function addType($type, $voclass, $value_cb, $key_cb = null, $vo_cb = null)
    {
        $this->types[$type] = array(
            'voclass' => $voclass,
            'value_callback' => $value_cb,
            'key_callback' => $key_cb,
            'vo_callback' => $vo_cb,
        );
    }

    public function getType($type = 'default')
    {
        $params = array_get($this->getTypes(), $type, $this->getDefaultType());
        foreach($params as $k => $v) {
            if (stristr($k, 'callback')) {
                if (is_string($v)) {
                    $params[$k] = array($this, $v);
                }
            }
        }
        return $params;
    }

    public function getTypes()
    {
        return $this->types;
    }

    public function getDefaultType()
    {
        return array(
            'voclass' => 'Smorken\Report\Manual\Models\VO',
            'value_callback' => function($k, $v) { return floatval($v); },
        );
    }

    public function getHandler()
    {
        return $this->handler;
    }

    public function setHandler(Handler $handler)
    {
        $this->handler = $handler;
    }
}