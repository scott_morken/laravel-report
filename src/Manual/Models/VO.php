<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 3/2/15
 * Time: 8:37 AM
 */

namespace Smorken\Report\Manual\Models;


use Smorken\Report\Manual\Contracts\Model;

class VO implements Model
{
    protected $label;
    protected $values = array();
    protected $data;

    public function __construct($label = null, $data = null)
    {
        $this->label = $label;
        $this->data = $data;
    }

    public function getLabel()
    {
        return $this->label;
    }

    public function setLabel($label)
    {
        $this->label = $label;
    }

    public function getData()
    {
        return $this->data;
    }

    public function setData($data)
    {
        $this->data = $data;
    }

    public function getValues()
    {
        return $this->values;
    }

    public function setValues($values)
    {
        $this->values = $values;
    }

    public function add($value)
    {
        if (is_null($value)) {
            return;
        }
        if (is_object($value)) {
            $value = (string) $value;
        }
        $i = floatval($value);
        $this->values[] = $i;
    }

    public function sum()
    {
        return array_sum($this->values);
    }

    public function min()
    {
        return min($this->values);
    }

    public function max()
    {
        return max($this->values);
    }

    public function count()
    {
        return count($this->values);
    }

    public function avg()
    {
        $count = $this->count();
        if ($count > 0) {
            return number_format($this->sum() / $count, 2);
        }
        return 0;
    }
}