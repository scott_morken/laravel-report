<?php
use Illuminate\Support\Facades\DB;
use Smorken\Report\Model;

/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 10/10/14
 * Time: 9:44 AM
 */

use Mockery as m;

class ModelTest extends \ReportTestCase {

    /**
     * @var \Smorken\Report\Model
     */
    protected $sut;

    public function setUp()
    {
        parent::setUp();
        $this->sut = new Model();
    }

    public function testHasManager()
    {
        $this->assertInstanceOf('\Smorken\Report\ReportManager', $this->sut->getManager());
    }

    public function testExtBuilderCreatesBuilder()
    {
        $m = m::mock('\Illuminate\Database\Query\Builder');
        $m->shouldReceive('newQuery')->andReturn('fooquery');
        $m->shouldReceive('getGrammar')->andReturn('foo');
        $this->assertInstanceOf('\Smorken\Report\Builder', $this->sut->extBuilder($m)->getBuilder());
    }

    public function testExtBuilderChainToBuilder()
    {
        $m = m::mock('\Illuminate\Database\Query\Builder');
        $q = m::mock('\Illuminate\Database\Query\Builder');
        $m->shouldReceive('newQuery')->andReturn($q);
        $g = m::mock('\Smorken\Report\Grammar');
        $g->shouldReceive('wrap')->andReturnUsing(function($p) { return $p[0]; });
        $m->shouldReceive('getGrammar')->andReturn($g);
        $q->shouldReceive('addSelect')->andReturn('foo');
        $this->assertInstanceOf('\Smorken\Report\Builder', $this->sut->extBuilder($m)->aggregate('count'));
    }

    public function testExtBuilderChainToExtBuilder()
    {
        $m = m::mock('\Illuminate\Database\Query\Builder');
        $q = m::mock('\Illuminate\Database\Query\Builder');
        $m->shouldReceive('newQuery')->andReturn($q);
        $m->shouldReceive('getGrammar')->andReturn('foo');
        $q->shouldReceive('bar')->andReturn($q);
        $r = $this->sut->extBuilder($m)->bar();
        $this->assertEquals($this->sut->getBuilder(), $r);
    }

    public function testExtBuilderChainNotExtBuilderToValue()
    {
        $m = m::mock('\Illuminate\Database\Query\Builder');
        $q = m::mock('\Illuminate\Database\Query\Builder');
        $m->shouldReceive('newQuery')->andReturn($q);
        $m->shouldReceive('getGrammar')->andReturn('foo');
        $q->shouldReceive('bar')->andReturn('biz');
        $r = $this->sut->extBuilder($m)->bar();
        $this->assertEquals('biz', $r);
    }
} 