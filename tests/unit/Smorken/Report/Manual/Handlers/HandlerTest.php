<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 3/2/15
 * Time: 10:04 AM
 */

use Smorken\Report\Manual\Handlers\IterableHandler;
use Mockery as m;

class HandlerTest extends PHPUnit_Framework_TestCase {

    /**
     * @var IterableHandler
     */
    protected $sut;

    /**
     * @var Mockery\Mock
     */
    protected $items;

    public function setUp()
    {
        $this->items = m::mock('Smorken\Report\Manual\Contracts\Items');
        $this->sut = new IterableHandler($this->items);
    }

    public function tearDown()
    {
        m::close();
    }

    public function testRunWithEmptyItems()
    {
        $this->items->shouldReceive('getItems')->andReturn([]);
        $voclass = '\Smorken\Report\Manual\Models\VO';
        $cb = function($k, $v) { return $v; };
        $this->assertEquals([], $this->sut->run($voclass, $cb));
    }

    public function testRunWithSimpleData()
    {
        $data = [
            'foo' => 1,
        ];
        $this->items->shouldReceive('getItems')->andReturn($data);
        $voclass = '\Smorken\Report\Manual\Models\VO';
        $cb = function($k, $v) { return $v; };
        $r = $this->sut->run($voclass, $cb);
        $this->assertInstanceOf($voclass, $r[0]);
        $this->assertCount(1, $r);
    }

    public function testRunWithSimpleDataIntegration()
    {
        $data = [
            'foo' => 1,
        ];
        $this->items->shouldReceive('getItems')->andReturn($data);
        $voclass = '\Smorken\Report\Manual\Models\VO';
        $cb = function($k, $v) { return $v; };
        $r = $this->sut->run($voclass, $cb);
        $this->assertEquals([1.0], $r[0]->getValues());
    }

    public function testRunCallbackWithSimpleDataIntegration()
    {
        $data = [
            'foo' => 1,
        ];
        $this->items->shouldReceive('getItems')->andReturn($data);
        $voclass = '\Smorken\Report\Manual\Models\VO';
        $cb = function($k, $v) { return $v + 2; };
        $r = $this->sut->run($voclass, $cb);
        $this->assertEquals([3.0], $r[0]->getValues());
    }

    public function testRunWithSimpleDataKeyIntegration()
    {
        $data = [
            'foo' => 1,
        ];
        $this->items->shouldReceive('getItems')->andReturn($data);
        $voclass = '\Smorken\Report\Manual\Models\VO';
        $cb = function($k, $v) { return $v; };
        $kcb = function($voclass, $k, $v) { return $k; };
        $r = $this->sut->run($voclass, $cb, $kcb);
        $this->assertArrayHasKey('foo', $r);
    }

    public function testRunWithSimpleDataSumIntegration()
    {
        $data = [
            1,
            2,
            3
        ];
        $this->items->shouldReceive('getItems')->andReturn($data);
        $voclass = '\Smorken\Report\Manual\Models\VO';
        $cb = function($k, $v) { return $v; };
        $r = $this->sut->run($voclass, $cb);
        $this->assertEquals(6, $r[0]->sum());
    }

    public function testRunWithComplexDataSumIntegration()
    {
        $data = [
            [
                'name' => 'foo',
                'value' => 1
            ],
            [
                'name' => 'foo',
                'value' => 1
            ],
            [
                'name' => 'bar',
                'value' => 1
            ],
        ];
        $this->items->shouldReceive('getItems')->andReturn($data);
        $voclass = '\Smorken\Report\Manual\Models\VO';
        $cb = function($k, $v) { return $v['value']; };
        $kcb = function($voclass, $k, $v) { return $v['name']; };
        $r = $this->sut->run($voclass, $cb, $kcb);
        $this->assertArrayHasKey('foo', $r);
        $this->assertEquals(2, $r['foo']->sum());
    }

    public function testRunVOCallbackWithComplexDataIntegration()
    {
        $data = [
            [
                'name' => 'foo',
                'value' => 1,
                'label' => 'fizz'
            ],
            [
                'name' => 'foo',
                'value' => 1,
                'label' => 'fizz'
            ],
            [
                'name' => 'bar',
                'value' => 1,
                'label' => 'fuzz',
            ],
        ];
        $this->items->shouldReceive('getItems')->andReturn($data);
        $voclass = '\Smorken\Report\Manual\Models\VO';
        $cb = function($k, $v) { return $v['value']; };
        $kcb = function($voclass, $k, $v) { return $v['name']; };
        $vocb = function($k, $v, $vo) { $vo->setLabel($v['label']); };
        $r = $this->sut->run($voclass, $cb, $kcb, $vocb);
        $this->assertArrayHasKey('foo', $r);
        $this->assertEquals('fizz', $r['foo']->getLabel());
    }
}