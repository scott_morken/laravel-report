<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 3/2/15
 * Time: 10:29 AM
 */

use Smorken\Report\Manual\Results\Result;
use Mockery as m;

class ResultTest extends PHPUnit_Framework_TestCase {

    /**
     * @var Result
     */
    protected $sut;

    /**
     * @var Mockery\Mock
     */
    protected $handler;

    public function setUp()
    {
        $this->handler = m::mock('\Smorken\Report\Manual\Contracts\Handler');
        $this->sut = new Result($this->handler);
    }

    public function tearDown()
    {
        m::close();
    }

    public function testAddTypesSetsDefaults()
    {
        $types = [
            'test' => [
                'voclass' => 'Smorken\Report\Manual\Models\VO',
                'value_callback' => 'vc',
                'key_callback' => 'kc',
            ],
        ];
        $this->sut->addTypes($types);
        $this->assertNull($this->sut->getType('test')['vo_callback']);
        $this->assertEquals(array($this->sut, 'vc'), $this->sut->getType('test')['value_callback']);
    }

    public function testRunDefault()
    {
        $this->handler->shouldReceive('run')
            ->with('Smorken\Report\Manual\Models\VO', '\Closure')
            ->andReturn('foo');
        $this->assertEquals('foo', $this->sut->run());
    }

    public function testRunWithTypes()
    {
        $types = [
            'test' => [
                'voclass' => 'Smorken\Report\Manual\Models\VO',
                'value_callback' => 'vc',
                'key_callback' => 'kc',
            ],
        ];
        $this->sut->addTypes($types);
        $this->handler->shouldReceive('run')
            ->with(
                'Smorken\Report\Manual\Models\VO',
                [
                    $this->sut,
                    'vc'
                ],
                [
                    $this->sut,
                    'kc'
                ],
                null
            )
            ->andReturn('foo');
        $this->assertEquals('foo', $this->sut->run('test'));
    }
}