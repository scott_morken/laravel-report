<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 3/2/15
 * Time: 9:46 AM
 */

use Smorken\Report\Manual\Items\Iterable;
use Mockery as m;

class IterableTest extends PHPUnit_Framework_TestCase {

    /**
     * @var Iterable
     */
    protected $sut;

    public function setUp()
    {
        $this->sut = new Iterable();
    }

    public function testGetDefaultItems()
    {

        $this->assertEquals([], $this->sut->getItems());
    }

    public function testGetItemsBySet()
    {
        $items = ['foo' => 'bar'];
        $this->sut->setItems($items);
        $this->assertEquals($items, $this->sut->getItems());
    }
}