<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 10/10/14
 * Time: 9:27 AM
 */

use Mockery as m;

class ReportFactoryTest extends \ReportTestCase {

    /**
     * @var \Smorken\Report\ReportFactory
     */
    protected $sut;

    public function setUp()
    {
        parent::setUp();
        $this->sut = $this->app['smorken.report.factory'];
    }

    public function testGetExternalGrammarFromBuilder()
    {
        $m = m::mock('\Illuminate\Database\Query\Builder');
        $m->shouldReceive('getGrammar')->andReturn('foo');
        $this->assertEquals('foo', $this->sut->getExternalGrammar($m));
    }

    public function testGetExternalGrammarFromModel()
    {
        $m = m::mock('\Illuminate\Database\Eloquent\Model');
        $b = m::mock('\Illuminate\Database\Query\Builder');
        $m->shouldReceive('newQuery')->andReturn($b);
        $b->shouldReceive('getGrammar')->andReturn('foo');
        $this->assertEquals('foo', $this->sut->getExternalGrammar($m));
    }

    public function testGetExternalGrammarThrowsException()
    {
        $m = 'string';
        $this->setExpectedException('\Smorken\Report\ReportException');
        $this->sut->getExternalGrammar($m);
    }

    public function testGetGrammarFromExisting()
    {
        $g = m::mock('\Illuminate\Database\Query\Grammars\MySqlGrammar');
        $this->assertInstanceOf('\Smorken\Report\Grammar\MySqlGrammar', $this->sut->getGrammarFromBaseClass('MySqlGrammar', $g));
    }

    public function testGetGrammarBase()
    {
        $g = m::mock('\Illuminate\Database\Query\Grammars\MySqlGrammar');
        $this->assertInstanceOf('\Smorken\Report\Grammar', $this->sut->getGrammar($g));
    }

    public function testMakeBuilder()
    {
        $q = m::mock('\Illuminate\Database\Query\Builder');
        $m = m::mock('\Illuminate\Database\Query\Builder');
        $m->shouldReceive('newQuery')->andReturn($q);
        $m->shouldReceive('getGrammar')->andReturn('foo');
        $b = $this->sut->makeBuilder($m);
        $this->assertInstanceOf('\Smorken\Report\Builder', $b);
    }
} 