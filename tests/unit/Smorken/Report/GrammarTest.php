<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 10/10/14
 * Time: 10:02 AM
 */

use Mockery as m;
use Illuminate\Support\Facades\DB;

class GrammarTest extends \ReportTestCase {

    /**
     * @var \Smorken\Report\Grammar
     */
    protected $sut;

    public function setUp()
    {
        parent::setUp();
        $this->extgrammar = m::mock('\Illuminate\Database\Query\Grammars\Grammar')->makePartial();
        $this->sut = new \Smorken\Report\Grammar($this->extgrammar);
    }

    public function testCompileAggregate()
    {
        $builder = m::mock('\Smorken\Report\Builder');
        $aggregate = m::mock('\Smorken\Report\Aggregate');
        $aggregate->shouldReceive('getColumn')->andReturn('foo');
        $aggregate->shouldReceive('getColumnAlias')->andReturn('fooalias');
        $aggregate->shouldReceive('getLabelColumn')->andReturn('bar');
        $aggregate->shouldReceive('getLabelAlias')->andReturn('baralias');
        $aggregate->shouldReceive('getType')->andReturn('type');
        $expected = array(
            'type("foo") as "fooalias"',
            '"bar" as "baralias"'
        );
        $this->assertEquals($expected, $this->sut->compileAggregateExt($builder, $aggregate));
    }

}