<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 3/2/15
 * Time: 12:03 PM
 */
return [
    1 => [
        'survey_id' => 1,
        'survey' => ['name' => 'Survey 1'],
        'question_id' => 1,
        'question' => ['name' => 'Question 1'],
        'value' => 1
    ],
    2 => [
        'survey_id' => 1,
        'survey' => ['name' => 'Survey 1'],
        'question_id' => 2,
        'question' => ['name' => 'Question 2'],
        'value' => 2
    ],
    3 => [
        'survey_id' => 1,
        'survey' => ['name' => 'Survey 1'],
        'question_id' => 3,
        'question' => ['name' => 'Question 3'],
        'value' => 3
    ],
    4 => [
        'survey_id' => 1,
        'survey' => ['name' => 'Survey 1'],
        'question_id' => 1,
        'question' => ['name' => 'Question 1'],
        'value' => 1
    ],
    5 => [
        'survey_id' => 1,
        'survey' => ['name' => 'Survey 1'],
        'question_id' => 2,
        'question' => ['name' => 'Question 2'],
        'value' => 2
    ],
    6 => [
        'survey_id' => 1,
        'survey' => ['name' => 'Survey 1'],
        'question_id' => 3,
        'question' => ['name' => 'Question 3'],
        'value' => 3
    ],
    7 => [
        'survey_id' => 1,
        'survey' => ['name' => 'Survey 1'],
        'question_id' => 1,
        'question' => ['name' => 'Question 1'],
        'value' => 1
    ],
    8 => [
        'survey_id' => 1,
        'survey' => ['name' => 'Survey 1'],
        'question_id' => 2,
        'question' => ['name' => 'Question 2'],
        'value' => 2
    ],
    9 => [
        'survey_id' => 1,
        'survey' => ['name' => 'Survey 1'],
        'question_id' => 3,
        'question' => ['name' => 'Question 3'],
        'value' => 3
    ],
];