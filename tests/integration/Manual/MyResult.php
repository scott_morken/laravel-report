<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 3/2/15
 * Time: 12:03 PM
 */

use Smorken\Report\Manual\Results\Result;
use Smorken\Report\Manual\Contracts\Model;

class MyResult extends Result {

    protected $types = [
        'report_type' => [
            'voclass' => 'Smorken\Report\Manual\Models\VO',
            'value_callback' => 'valueCallbackOne',
            'key_callback' => 'keyCallbackOne',
            'vo_callback' => 'voCallbackOne',
        ],
        'chart_type' => [
            'voclass' => 'Smorken\Report\Manual\Models\VO',
            'value_callback' => 'valueCallbackTwo',
            'key_callback' => 'keyCallbackTwo',
            'vo_callback' => 'voCallbackTwo',
        ],
    ];


     public function valueCallbackOne($key, $data)
     {
        $value = $data['value'];
        if ($value !== null && $value == floatval($value)) {
            return $value;
        }
        return false;
     }

     public function keyCallbackOne($voclass, $key, $data)
     {
        return $data['question_id'];
     }

     public function voCallbackOne($key, $data, Model $model)
     {
        $model->setData($data['question']);
        $model->setLabel($data['question']['name']);
         return $model;
     }


    public function valueCallbackTwo($key, $data)
    {
        $value = $data['value'];
        if ($value !== null && $value == floatval($value)) {
            return $value;
        }
        return false;
    }

    public function keyCallbackTwo($voclass, $key, $data, &$results)
    {
        $survey_id = $data['survey_id'];
        $q_id = $data['question_id'];
        if (!isset($results[$survey_id])) {
            $results[$survey_id] = [
                'name' => $data['survey']['name'],
                'data' => [],
            ];
        }
        if (!isset($results[$survey_id]['data'][$q_id])) {
            $results[$survey_id]['data'][$q_id] = new $voclass;
        }
        return $results[$survey_id]['data'][$q_id];
    }

    public function voCallbackTwo($key, $data, Model $model)
    {
        $model->setData($data['question']);
        $model->setLabel($data['question']['name']);
        return $model;
    }
}