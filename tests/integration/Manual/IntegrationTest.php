<?php
/**
 * Created by IntelliJ IDEA.
 * User: smorken
 * Date: 3/2/15
 * Time: 12:05 PM
 */

include __DIR__ . '/MyResult.php';

use Smorken\Report\Manual as srm;

class IntegrationTest extends PHPUnit_Framework_TestCase {

    public function testGetResultsReport()
    {
        $data = include __DIR__ . '/data.php';
        $items = new srm\Items\Iterable($data);
        $handler = new srm\Handlers\IterableHandler($items);
        $builder = new MyResult($handler);
        $results = $builder->run('report_type');
        //print_r($results);
        foreach($results as $i => $vo) {
            $this->assertEquals($i, $vo->avg());
        }
    }

    public function testGetResultsChart()
    {
        $data = include __DIR__ . '/data.php';
        $items = new srm\Items\Iterable($data);
        $handler = new srm\Handlers\IterableHandler($items);
        $builder = new MyResult($handler);
        $results = $builder->run('chart_type');
        print_r($results);
        $avgs = [];
        foreach($results as $survey_id => $survey_data) {
            foreach($survey_data['data'] as $question_id => $vo) {
                $avgs[] = $vo->avg();
            }
        }
        $this->assertEquals([1, 2, 3], $avgs);
    }
}